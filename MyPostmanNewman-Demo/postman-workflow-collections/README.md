# _`Workflows`_ - _`Automations`_ & _`Testing`_

- Read : https://tideaccount.atlassian.net/wiki/spaces/BOR/pages/2351136773/Postman+Automation+of+Integration-Testing+for+Workflows

### Install Newman
 
- Read : https://learning.postman.com/docs/running-collections/using-newman-cli/command-line-integration-with-newman/

- Install (NVM -> NodeJS -> NPM )
- Install Newman

```shell script

npm install -g newman

```

### Run Newman

```shell script

newman run mycollection.json

``` 

#### Newman Options

```shell script

# Giving data file from command line

-d data_file_either_csv_or_json.[csv,json]



# Reporting (cli or html)

-r cli,html

```