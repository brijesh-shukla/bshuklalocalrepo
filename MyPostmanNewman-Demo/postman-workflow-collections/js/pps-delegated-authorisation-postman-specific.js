
pm.environment.set("API-KEY", fetchPPSApiKey());
pm.environment.set("card-serial-number", fetchCardSerialNumber());
pm.environment.set("card-mask-pan", fetchCardMaskPan());

pm.environment.set("txn_id",
    function(){
      return generateXDigitTxnId(21);
    }()
);

pm.environment.set("txn-amount",
    function(){
      return generateXDigitTxnId(3);
    }()
);

pm.environment.set("txn-version",
    function(){
      return generateXDigitTxnId(3);
    }()
);