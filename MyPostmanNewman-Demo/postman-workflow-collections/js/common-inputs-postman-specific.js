
pm.environment.set("scheme", fetchScheme());
pm.environment.set("host", fetchHost());
pm.environment.set("port", fetchPort());

pm.environment.set("teamMemberId", fetchUserId());
pm.environment.set("accountId", fetchCompanyId());
pm.environment.set("limitType", fetchLimitType());

pm.environment.set("asOfUTCDateTime", fetchCurrentUTCTime());

var inputJSONdata = pm.iterationData.get("body-data");

pm.environment.set("envInputJSONdata", JSON.stringify(inputJSONdata));

pm.sendRequest(fetchJWTServerUrl(),
    (error, response) => {
      if (error) {
        console.log(error);
      } else {
        var res = response.json();
        pm.environment.set("Token", res.token);
      }
    }
);

pm.environment.set("happy-coding",
    function () {
      return "coding..."
    }()
);
