
function fetchPPSApiKey(){
  return "secret-key";
}

function fetchCardMaskPan() {
  return "\"530553******9311\"";
}

function fetchCardSerialNumber() {
  return "2010057592";
}

function digitsZeroToNine() {
  return [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
}

function randomNumberGenerator(min, max) {
  return Math.floor(
      // random b/w min-max inclusive
      Math.random() * (max - min + 1) + min
  );
}

function generateXDigitTxnId(digitsNeeded) {
  let digits = digitsZeroToNine();

  // avoid 20 digit txn-id as first digit should not be zero
  txnId = digits[randomNumberGenerator(1, 9)];

  for (digitCounter = 1; digitCounter < digitsNeeded; digitCounter++) {
    txnId += "" + digits[randomNumberGenerator(0, 9)];
  }
  return txnId;
}