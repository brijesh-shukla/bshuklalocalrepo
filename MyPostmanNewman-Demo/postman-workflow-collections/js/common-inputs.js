function fetchScheme() {
  return 'http';
}

function fetchHost() {
  return 'localhost';
}

function fetchPort() {
  return '8080';
}

function fetchUserId() {
  return 45678;
}

function fetchCompanyId() {
  return 100;
}

function fetchLimitType() {
  return "MONTHLY";
}

function fetchCurrentUTCTime(){
  return new Date().toISOString();
}


function fetchJWTRoles() {
  return ["READER", "ACCESS_KEY", "ACTIVE_INSTALLATION", "WEB_ACCESS_KEY"]
  .join("&roles=");
}

function fetchJWTFeatureToggles() {
  return ["PPS_SECONDARY_ACCOUNT"]
  .join("&enabledFeatureToggleNames=");
}

function fetchJWTServerUrl() {

  // Use your local JWT server URL
  return "http://localhost:9742/mockedJwt/token"
  .concat("/companyId/").concat(fetchCompanyId())
  .concat("/userId/").concat(fetchUserId())
  .concat("?")
  .concat("includeBearerPrefix=false")
  .concat("&enabledFeatureToggleNames=")
  .concat(fetchJWTFeatureToggles())
  .concat("&roles=")
  .concat(fetchJWTRoles())
}