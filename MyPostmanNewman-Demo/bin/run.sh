#!/usr/bin/env sh

JWT_SERVER_PORT=9742 JWT_MGMT_SERVER_PORT=9741 java -jar /app/lib/mockJWTSimulator-0.0.1-SNAPSHOT.jar &

java -Dspring.profiles.active=dev -Duser.timezone=UTC -jar /app/lib/domestic-txn-authz-1.0-SNAPSHOT.jar

