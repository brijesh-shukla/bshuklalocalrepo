## Docker Compose Collections

### What we already have

- mySQL - 5.6  [Used for `JBE`]
- mySQL - 5.7  [Newer services in UK, Ex : `Auth-Service`, `Account-Rest-Service` etc]
- postgres - 11.8 [All new stuff, Ex : `India Alpha` , `Domestic-Txn-Auth-Service/Lambda`]
- SFTP     - Mimic real action with SFTP for PPS-Legacy-SFTP

### What is Coming soon...

- Local SQS
- Local Gravitee
- Local httpd
- Local nginx (SSL Termination)
- Local Consul / Envoy proxy to local discovery
- Local NodeJS for JWT Generator // Look of for the Java `jwtMockGenertor`