var Swagger2Postman = require("swagger2-postman-generator");

const convertor = Swagger2Postman.convertSwagger().fromUrl(
    "http://localhost:8080/v2/api-docs");

convertor.toPostmanCollectionFile("domestic-txn-authorisation.json");