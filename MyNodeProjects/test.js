
function digitsZeroToNine() {
    return [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
}

function randomNumberGenerator(min, max) {
    return Math.floor(
        // random b/w min-max inclusive
        Math.random() * (max - min + 1) + min
    );
}

pm.environment.set("genXDigitTxnId", function genXDigitTxnId() {
    let digits = pm.environment.get("digitsZeroToNine");
    console.log(digits);
    var txnId = digits[randomNumberGenerator(0, 9)] + 1;
    for (var i = 0; i < 20; i++) {
        txnId += randomNumberGenerator(0, 9);
    }
    console.log(txnId);
}())


digitsZeroToNine();