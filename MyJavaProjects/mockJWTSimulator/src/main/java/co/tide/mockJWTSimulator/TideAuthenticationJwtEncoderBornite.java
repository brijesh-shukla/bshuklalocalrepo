package co.tide.mockJWTSimulator;

import co.tide.security.auth.jwt.AuthenticationConfig;
import co.tide.security.auth.jwt.AuthenticationMetadata;
import co.tide.security.auth.jwt.JwtSigningException;
import co.tide.security.auth.jwt.TideAuthenticationJwtEncoder;
import co.tide.security.auth.jwt.insecure.InsecureMacSigner;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import java.time.Clock;
import java.time.ZonedDateTime;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.couchbase.CouchbaseProperties.Env;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import static java.time.format.DateTimeFormatter.ISO_ZONED_DATE_TIME;
import static java.time.temporal.ChronoUnit.SECONDS;

@Primary
@Component("TideAuthenticationJwtEncoderBornite")
public class TideAuthenticationJwtEncoderBornite extends TideAuthenticationJwtEncoder {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private final Clock clock;

    private final AuthenticationConfig authenticationConfig;

    private final JWSSigner jwsSigner;

    private final Environment environment;

    @Autowired
    public TideAuthenticationJwtEncoderBornite(AuthenticationConfig authenticationConfig, Clock clock, Environment environment) {
        super(authenticationConfig, clock);
        this.authenticationConfig = authenticationConfig;
        this.environment = environment;

        final String secret = environment.getRequiredProperty("JWT_AUTH_SECRET");
        this.authenticationConfig.setJwtSecret(secret);

        System.out.println("\n \n \n ------------");
        System.out.println("Secret : " + this.authenticationConfig.getJwtSecret());
        System.out.println("\n \n \n ------------");

        this.clock = clock;
        this.jwsSigner = createSigner();
    }

    /**
     * WIP uses a very short secret for this (you know what it is). The legacy encoder and decoder were perfectly happy
     * with such a short key, but Nimbus will reject it. Instead we have a custom signer implementation that skips the
     * key length checks. But it must be explicitly enabled.
     *
     * @return the JWS signer
     */
    private JWSSigner createSigner() {
        int secretLength = authenticationConfig.getJwtSecret().getBytes().length;
        if (authenticationConfig.isAllowInsecureSecrets() && secretLength < 32) {
            return new InsecureMacSigner(authenticationConfig.getJwtSecret());
        } else {
            try {
                return new MACSigner(authenticationConfig.getJwtSecret());
            } catch (KeyLengthException e) {
                throw new IllegalArgumentException(
                        "Key is too short. Set tide.service.auth.allowInsecureSecrets=true for WIP/Staging?", e);
            }
        }
    }

    @Override
    public String generateAuthenticationHeader(AuthenticationMetadata authenticationMetadata) {

        ZonedDateTime now = ZonedDateTime.now(clock);
        ZonedDateTime expiry = now.plus(authenticationConfig.getJwtTimeToLive(), SECONDS);
        Date iat = Date.from(now.toInstant());

        String dataClaim = generateDataClaim(authenticationMetadata);

        // Setting the audience would be good, as the called service could validate that the JWT was intended
        // for them. But it would probably be pretty fiddly (particularly at the gateway level), and the callers
        // would have to know the _name_ of the service they are calling. Maybe something for v3.
        // Similarly, the issuer should really indicate the service that generated the token and the environment
        // (e.g. "support_service_staging"). Unfortunately, the legacy decoders require it to be the string "tide".
        // Setting the user identifier in the data claim would be preferable but that breaks backward compatibility
        // for services still using the legacy JWT library. In the future this claim will never be null but during the
        // transition period from our existing auth mechanism to OIDC this can be null.
        JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                // Standard claims
                .issuer("tide")
                .issueTime(iat)
                .notBeforeTime(iat)
                .expirationTime(Date.from(expiry.toInstant()))
                // Custom claims
                .claim("timeToLive", ISO_ZONED_DATE_TIME.format(expiry)) // TTL FML
                .claim("version", 2)
                .claim("requestId", authenticationMetadata.getRequestId())
                .claim("companyId", authenticationMetadata.getCompanyId().orElse(null))
                .claim("credentialId", authenticationMetadata.getCredentialId().orElse(null))
                .claim("staffUserId", authenticationMetadata.getStaffUserId().orElse(null))
                .claim("data", dataClaim)
                .claim("enabledFeatureToggleNames", authenticationMetadata.getEnabledFeatureToggleNames())
                .claim("userId", authenticationMetadata.getUserIdentifier().orElse(null))
                .claim("principalUserId", authenticationMetadata.getUserIdentifier().orElse(null))
                //.claim("userIdentifier", authenticationMetadata.getUserIdentifier().orElse(null))
                .build();

        SignedJWT signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.HS256), claimsSet);
        try {
            signedJWT.sign(jwsSigner);
        } catch (JOSEException e) {
            throw new JwtSigningException("Couldn't sign JWT", e);
        }

        return "Bearer " + signedJWT.serialize();
    }

    private String generateDataClaim(AuthenticationMetadata authenticationMetadata) {
        ObjectNode userNode = OBJECT_MAPPER.createObjectNode();
        userNode.put("id", authenticationMetadata.getUserId() == null ? 0 : authenticationMetadata.getUserId());

        if (StringUtils.hasText(authenticationMetadata.getUserType())) {
            userNode.put("type", authenticationMetadata.getUserType());
        }

        ObjectNode installationNode = OBJECT_MAPPER.createObjectNode();
        installationNode.put("id",
                authenticationMetadata.getInstallationId() == null ? 0 : authenticationMetadata.getInstallationId());

        ArrayNode authoritiesNode = OBJECT_MAPPER.createArrayNode();
        authenticationMetadata.getAuthorities().forEach(a -> authoritiesNode.add(a.getAuthority()));

        ObjectNode dataNode = OBJECT_MAPPER.createObjectNode();
        dataNode.set("user", userNode);
        dataNode.set("installation", installationNode);
        dataNode.set("authorities", authoritiesNode);

        //ObjectNode request = OBJECT_MAPPER.createObjectNode();
        //request.put("requestId", UUID.randomUUID().toString());
        // request.put("origin", "CLIENT");
        // dataNode.set("request", request);

        return dataNode.toString();
    }

}
