package co.tide.mockJWTSimulator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class LocalAuthenticationMetaData {

    @ApiModelProperty(example = "02a47079-a37f-4c30-ac10-c40b50237176", position = 0)
    private String requestId;

    @ApiModelProperty(example = "false", position = 1)
    private boolean includeBearerPrefix;

    @ApiModelProperty(position = 2)
    private Integer userId;

    @ApiModelProperty(example = "123/UUID", position = 3)
    private String companyId;

    @ApiModelProperty(example = "000", position = 4)
    private Integer installationId;

    @ApiModelProperty(position = 5)
    private String userIdentifier;

    @ApiModelProperty(example = "test@tide.co", position = 6)
    private String username;

    @ApiModelProperty(example = "NORMAL_USER", allowableValues = "NORMAL_USER, BATCH_JOB, STAFF_USER, OPEN_BANKING_GATEWAY",
            position = 7)
    private String userType;

    @ApiModelProperty(position = 8)
    private String authToken;

    @ApiModelProperty(position = 9)
    private String staffUserId;

    @ApiModelProperty(allowableValues = "READER,"
            + " ACCESS_KEY,"
            + " REGISTRATION,"
            + "ACTIVE_INSTALLATION,"
            + " INSTALLATION,"
            + " WEB_ACCESS_KEY, "
            + "CONSENT_ACCESS_KEY, "
            + "ACCESS_KEY,CLIENT_KEY", position = 10)
    private List<String> grantedAuthorities;

    @ApiModelProperty(position = 20)
    private Set<String> enabledFeatureToggleNames;

}
