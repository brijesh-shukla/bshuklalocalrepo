package co.tide.mockJWTSimulator.config;

import co.tide.security.rest.RestAuthenticationConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

/**
 * General configuration of the application.
 */
@ComponentScan(basePackages = {"co.tide"})
@Import({
        SwaggerConfig.class,
        RestAuthenticationConfig.class
})
public class AppConfig {

}
