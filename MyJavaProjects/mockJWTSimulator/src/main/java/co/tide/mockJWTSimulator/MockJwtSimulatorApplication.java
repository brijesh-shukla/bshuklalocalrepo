package co.tide.mockJWTSimulator;

import co.tide.mockJWTSimulator.config.AppConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication(exclude = {
        org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class,
        org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration.class
})
@Import(AppConfig.class)
public class MockJwtSimulatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(MockJwtSimulatorApplication.class, args);
    }

}
