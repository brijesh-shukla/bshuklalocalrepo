package co.tide.mockJWTSimulator;

import co.tide.security.auth.jwt.AuthenticationMetadata;
import co.tide.security.auth.jwt.InvalidBearerTokenException;
import co.tide.security.auth.jwt.TideAuthenticationJwtDecoder;
import co.tide.security.auth.jwt.TideAuthenticationJwtEncoder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.context.WebApplicationContext;

import static co.tide.security.rest.Roles.BATCH_JOB;

public abstract class JwtBase {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Autowired
    @Qualifier("TideAuthenticationJwtEncoderBornite")
    public TideAuthenticationJwtEncoder encoder;

    @Autowired
    @Qualifier("TideAuthenticationJwtEncoderBorniteLegacy")
    public TideAuthenticationJwtEncoderBorniteLegacy jbeVersionEncoder;

    @Autowired
    private TideAuthenticationJwtDecoder decoder;

    @Autowired
    private WebApplicationContext context;

    public TideAuthenticationJwtEncoder getEncoder(final boolean useLegacy) {
        if (useLegacy) {
            return jbeVersionEncoder;
        }
        return encoder;
    }

    protected String createServiceJwtWithoutUserId(final UUID reqUUID, final boolean useLegacy,
            final Set<String> enabledFeatureToggleNames) {
        final AuthenticationMetadata metadata = AuthenticationMetadata.newBuilder()
                .withRequestId(reqUUID.toString())
                .withUserType(BATCH_JOB)
                .withAuthorities(List.of(BATCH_JOB))
                .withStaffUserId("Staff-User-Local-jwt-123")
                .withEnabledFeatureToggleNames(enabledFeatureToggleNames)
                .build();
        final String jwt = getEncoder(useLegacy).generateAuthenticationHeader(metadata);

        try {
            System.out.println(decoder.extractAuthenticationMetadata(jwt));
        } catch (InvalidBearerTokenException e) {
            e.printStackTrace();
        }
        return jwt;
    }

    protected String createServiceJwtWithUserId(final UUID reqUUID, int userId, final boolean useLegacy,
            final Set<String> enabledFeatureToggleNames) {
        final AuthenticationMetadata metadata = AuthenticationMetadata.newBuilder()
                .withRequestId(reqUUID.toString())
                .withUserId(userId)
                .withUserIdentifier(String.valueOf(userId))
                .withCredentialId(String.valueOf(userId))
                .withUserType(BATCH_JOB)
                .withAuthorities(List.of(BATCH_JOB))
                .withStaffUserId("Staff-User-Local-jwt-123")
                .withEnabledFeatureToggleNames(enabledFeatureToggleNames)
                .build();

        final String jwt = getEncoder(useLegacy).generateAuthenticationHeader(metadata);

        try {
            System.out.println(decoder.extractAuthenticationMetadata(jwt));
        } catch (InvalidBearerTokenException e) {
            e.printStackTrace();
        }
        return jwt;
    }

    protected String createUserJwtWithIdAndRoles(final UUID reqUUID, final int installationId, final int userId,
            final String companyId, final boolean useLegacy, final Set<String> enabledFeatureToggleNames,
            final String... roles) {

        final AuthenticationMetadata metadata = AuthenticationMetadata.newBuilder()
                .withRequestId(reqUUID.toString())
                .withUserId(userId)
                .withUserIdentifier(String.valueOf(userId))
                .withCredentialId(String.valueOf(userId))
                .withCompanyId(companyId)
                .withInstallationId(installationId)
                .withUserType("NORMAL_USER")
                .withStaffUserId("Staff-User-Local-jwt-123")
                .withAuthorities(List.of(roles))
                .withEnabledFeatureToggleNames(enabledFeatureToggleNames)
                .build();

        final String jwt = getEncoder(useLegacy).generateAuthenticationHeader(metadata);

        try {
            System.out.println(decoder.extractAuthenticationMetadata(jwt));
        } catch (InvalidBearerTokenException e) {
            e.printStackTrace();
        }
        return jwt;
    }

    protected String doItAll(final LocalAuthenticationMetaData localAuthenticationMetaData, final boolean useLegacy) {

        final AuthenticationMetadata metadata = AuthenticationMetadata.newBuilder()
                .withRequestId(localAuthenticationMetaData.toString())
                .withUserId(localAuthenticationMetaData.getUserId())
                .withUserIdentifier(String.valueOf(localAuthenticationMetaData.getUserId()))
                .withCredentialId(String.valueOf(localAuthenticationMetaData.getUserId()))
                .withCompanyId(localAuthenticationMetaData.getCompanyId())
                .withInstallationId(localAuthenticationMetaData.getInstallationId())
                .withUserType(localAuthenticationMetaData.getUserType())
                .withAuthorities(localAuthenticationMetaData.getGrantedAuthorities())
                .withEnabledFeatureToggleNames(localAuthenticationMetaData.getEnabledFeatureToggleNames())
                .build();

        final String jwt = getEncoder(useLegacy).generateAuthenticationHeader(metadata);

        try {
            System.out.println(decoder.extractAuthenticationMetadata(jwt));
        } catch (InvalidBearerTokenException e) {
            e.printStackTrace();
        }
        return jwt;
    }

    protected String toJson(Object o) {
        try {
            return OBJECT_MAPPER.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Couldn't convert object to JSON", e);
        }
    }


}
