package co.tide.mockJWTSimulator.config;


import co.tide.security.auth.jwt.AuthenticationMetadata;
import java.util.Arrays;
import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 *
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    public static final String AUTHORIZATION_HEADER = "Authorization";

    private List<ApiKey> apiKey() {
        return Arrays.asList(new ApiKey("JWT", AUTHORIZATION_HEADER, "header"));
    }

    private SecurityContext securityContext() {
        AuthorizationScope[] scopes = {};
        return SecurityContext.builder()
                .securityReferences(Arrays.asList(SecurityReference.builder().reference("JWT").scopes(scopes).build()))
                .forPaths(PathSelectors.any())
                .build();
    }

    @Bean
    public Docket api() {
        var apiInfo = new ApiInfoBuilder()
                .title("Local JWT Generator Service")
                .version("1.0")
                .license(null)
                .licenseUrl(null)
                .termsOfServiceUrl(null)
                .description("Local JWT Generator Service")
                .build();

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo)
                .useDefaultResponseMessages(false)
                .securitySchemes(apiKey())
                .securityContexts(Arrays.asList(securityContext()))
                .ignoredParameterTypes(AuthenticationMetadata.class)
                .select()
                .apis(RequestHandlerSelectors
                        .basePackage("co.tide"))
                .paths(PathSelectors.any())
                .build();
    }
}