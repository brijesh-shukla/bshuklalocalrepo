package co.tide.mockJWTSimulator;

import co.tide.security.rest.annotations.Unauthenticated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(tags = "Mock Jwt Generator")
@RestController
@RequestMapping("/mockedJwt")
public class JwtGenerator extends JwtBase {

    @Unauthenticated
    @GetMapping(value = "/token/companyId/{companyId}/userId/{userId}", produces = {"application/json; charset=UTF-8"})
    public ResponseEntity<Map<String, String>> createUserJwtWithIdAndRoles(
            @ApiParam(example = "123") @PathVariable("userId") int userId,
            @ApiParam(example = "123") @PathVariable("companyId") String companyId,
            @ApiParam(example = "Director") @RequestParam("roles") Set<String> roles,
            @ApiParam(example = "PPS_SECONDARY_ACCOUNT") @RequestParam("enabledFeatureToggleNames") Set<String> enabledFeatureToggleNames,
            @ApiParam(example = "false") @RequestParam("includeBearerPrefix") boolean includeBearerPrefix,
            @ApiParam(example = "false") @RequestParam("useLegacy") boolean useLegacy
    ) {

        final String[] rolesArray = roles.toArray(new String[]{});

        final int installationId = 1;

        final String tokenWithBearerPrefix = createUserJwtWithIdAndRoles(
                UUID.randomUUID(),
                installationId,
                userId,
                String.valueOf(companyId),
                useLegacy,
                enabledFeatureToggleNames, rolesArray);

        return getMapResponseEntity(includeBearerPrefix, tokenWithBearerPrefix);

    }

    @Unauthenticated
    @GetMapping(value = "/token/serviceJwtWithoutUserId", produces = {"application/json; charset=UTF-8"})
    public ResponseEntity<Map<String, String>> createServiceJwtWithoutUserId(
            @ApiParam(example = "PPS_SECONDARY_ACCOUNT") @RequestParam("enabledFeatureToggleNames") Set<String> enabledFeatureToggleNames,
            @ApiParam(example = "false") @RequestParam("includeBearerPrefix") boolean includeBearerPrefix,
            @ApiParam(example = "false") @RequestParam("useLegacy") boolean useLegacy) {

        final String tokenWithBearerPrefix = createServiceJwtWithoutUserId(UUID.randomUUID(), useLegacy,
                enabledFeatureToggleNames);

        return getMapResponseEntity(includeBearerPrefix, tokenWithBearerPrefix);

    }

    @Unauthenticated
    @GetMapping(value = "/token/userId/{userId}", produces = {"application/json; charset=UTF-8"})
    public ResponseEntity<Map<String, String>> serviceJwtWithUserId(
            @ApiParam(example = "123") @PathVariable("userId") int userId,
            @ApiParam(example = "PPS_SECONDARY_ACCOUNT") @RequestParam("enabledFeatureToggleNames") Set<String> enabledFeatureToggleNames,
            @ApiParam(example = "false") @RequestParam("includeBearerPrefix") boolean includeBearerPrefix,
            @ApiParam(example = "false") @RequestParam("useLegacy") boolean useLegacy) {

        final String tokenWithBearerPrefix = createServiceJwtWithUserId(UUID.randomUUID(), userId, useLegacy,
                enabledFeatureToggleNames);

        return getMapResponseEntity(includeBearerPrefix, tokenWithBearerPrefix);

    }

    @Unauthenticated
    @PostMapping(path = "/token/toItAll")
    public ResponseEntity<Map<String, String>> doit(
            @RequestBody LocalAuthenticationMetaData localAuthenticationMetaData
    ) {

        final String tokenWithBearerPrefix = doItAll(localAuthenticationMetaData, false);

        return getMapResponseEntity(localAuthenticationMetaData.isIncludeBearerPrefix(), tokenWithBearerPrefix);

    }

    private ResponseEntity<Map<String, String>> getMapResponseEntity(final boolean includeBearerPrefix,
            final String tokenWithBearerPrefix) {

        if (includeBearerPrefix) {
            return ResponseEntity.ok(Map.of("token", tokenWithBearerPrefix));
        }
        final String tokenWithoutBearerPrefix = tokenWithBearerPrefix.replaceAll("Bearer ", "");
        return ResponseEntity.ok(Map.of("token", tokenWithoutBearerPrefix));
    }
}
