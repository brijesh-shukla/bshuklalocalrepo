package co.tide.clearbankmigration.camunda;

import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = {"co.tide.clearbankmigration"})
@EnableProcessApplication
public class AppConfig {

}
