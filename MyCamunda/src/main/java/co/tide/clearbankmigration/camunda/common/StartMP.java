package co.tide.clearbankmigration.camunda.common;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javax.inject.Inject;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.ManagementService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

@Service
@Setter
@Slf4j
public class StartMP implements JavaDelegate {

    @Inject
    private ManagementService managementService;

    @Inject
    private RuntimeService runtimeService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        log.info("Entering into process =====> : {} {} {} {}",
                CtbbMigrationCamundaHelper.getProperProcessName(execution.getCurrentActivityName()),
                execution.getProcessBusinessKey());

        log.info("Exiting from  process =====> : {}",
                CtbbMigrationCamundaHelper.getProperProcessName(execution.getCurrentActivityName()));

    }

    public String fixTimer(final DelegateExecution execution) {

        log.info(execution.getBusinessKey());

        log.info("Variables at clock {}", execution.getVariables());

        final String timerName = execution.getCurrentActivityName();

        log.info(timerName);
        LocalDateTime localDateTime = LocalDateTime.now(ZoneId.of("UTC"));
        localDateTime = localDateTime.plusMinutes(2);
        //System.out.println(DateTimeFormatter.ISO_ZONED_DATE_TIME.toString());
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        String time = localDateTime.format(format);
        log.info("{}, {}", time, LocalDateTime.now());
        execution.setVariable("Time-Fire-At", time);

        return time;
    }

    public List<Integer> getEngines(DelegateExecution execution) {
        return IntStream.range(101, 103).boxed().collect(Collectors.toList());
    }


}