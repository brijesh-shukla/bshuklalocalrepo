package co.tide.clearbankmigration.camunda.common;

import javax.annotation.Nonnull;

/**
 * Class has constant that map various Camunda objects names to constants needed in Process Application to trigger
 * things programmatically
 *
 * Note : Any change in the Camunda(BPMN) should be captured here and vice-versa
 */
public class CtbbMigrationCamundaHelper {

    public static final class Field {

        public static final String COMPANY_OR_MEMBER_ID = "companyOrMemberId";
        public static final String CURRENT_ACCOUNT_ID = "currentAccountId";
    }

    public static final String getProperProcessName(@Nonnull final String processName) {
        String newProcessName = processName.replaceAll("\n", " - ");
        return newProcessName;
    }

}
