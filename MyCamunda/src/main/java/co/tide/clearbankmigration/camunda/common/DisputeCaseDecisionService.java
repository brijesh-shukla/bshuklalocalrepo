package co.tide.clearbankmigration.camunda.common;

import javax.inject.Inject;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.ManagementService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

@Service
@Setter
@Slf4j
public class DisputeCaseDecisionService implements JavaDelegate {

    @Inject
    private ManagementService managementService;

    @Inject
    private RuntimeService runtimeService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        log.info("Entering into : {} {} {}",
                CtbbMigrationCamundaHelper.getProperProcessName(execution.getCurrentActivityName()),
                execution.getProcessBusinessKey(),
                execution.getVariablesLocal());

        execution.setVariable("STATUS", "CHARGEBACK");

        log.info("Exiting  from : {}",
                CtbbMigrationCamundaHelper.getProperProcessName(execution.getCurrentActivityName()));

    }

    public boolean chargeBack(final DelegateExecution execution) {

        final String decision = execution.getVariable("STATUS").toString();
        if (decision.equals("DISMISS")) {
            return false;
        }
        return true;
    }
}