package co.tide.clearbankmigration.camunda.common.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.ManagementService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.runtime.Job;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/v1/api/mgmt")
@Slf4j
public class CtbbRestAdminMgmtController {

    @Inject
    private RuntimeService runtimeService;

    @Inject
    private ManagementService managementService;

    @GetMapping("/jobs")
    @ResponseBody
    public List<Map<String, Object>> jobs() {

        log.info("Getting calls ....");
        List<Job> jobs = managementService.createJobQuery().orderByJobDuedate().asc().list();

        List<Map<String, Object>> jobDetails = new ArrayList<>();
        jobs.forEach(job -> {

            Map<String, Object> map = new HashMap<>();
            map.put("processDefinitionKey", job.getProcessDefinitionKey());
            map.put("processDefinitionId", job.getProcessDefinitionId());
            map.put("processInstanceId", job.getProcessInstanceId());
            map.put("jobDefinitionId", job.getJobDefinitionId());
            map.put("jobDeploymentId", job.getDeploymentId());
            map.put("executionId", job.getExecutionId());
            map.put("jobCreationTime", job.getCreateTime());
            map.put("jobPriority", job.getPriority());
            map.put("jobRetires", job.getRetries());
            map.put("jobDueDate", job.getDuedate());
            map.put("jobTenant", job.getTenantId());

            jobDetails.add(map);
        });
        return jobDetails;
    }

}
